extends Node2D


func _ready():
	$Control.modulate = Color(1, 1, 1, 0)
	yield(get_tree().create_timer(1.0), "timeout")
	$Tween.interpolate_property(
		$Control,
		"modulate",
		Color(1, 1, 1, 0),
		Color(1, 1, 1, 1),
		5,
		Tween.TRANS_QUAD,
		Tween.EASE_IN
	)
	$Tween.start()
	yield($Tween, "tween_all_completed")
	Game.change_scene("res://game/World.tscn")
