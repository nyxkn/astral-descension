extends Node

#export (String, FILE, "*.tscn") var NEW_GAME: String = "res://game/Game.tscn"

func _ready():
	# entry point for the whole application
	Config.loadConfig()
	
	Config.NEW_GAME = "res://game/screens/Intro.tscn"

	Game.change_scene(Config.NEW_GAME)
#    emit_signal("change_scene", MAIN_MENU)
	
#    get_tree().change_scene("res://game/MainMenu.tscn")
#    SceneManager.goto_scene("res://scenes/screens/MainMenu.tscn")
