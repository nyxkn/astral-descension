extends Area2D

#onready var Grid = get_parent()

#onready var Grid = get_node("../Level")

signal step_taken

onready var world = get_parent()
onready var move_tween = $MoveTween
onready var fall_tween = $FallTween
onready var unfall_tween = $UnfallTween
onready var stairs_tween = $StairsTween

var falling = false

var speed = 10

var tile_size = 32
var inputs = {"ui_right": Vector2.RIGHT,
			"ui_left": Vector2.LEFT,
			"ui_up": Vector2.UP,
			"ui_down": Vector2.DOWN}

var pause = true setget pause_set

var steps_counter = 0



func _ready():
	pass


# unhandled is the last input function called so it's helpful for letting controls be eaten by gui first or something
func _unhandled_input(event):
	if ! pause:
		for dir in inputs.keys():
			if event.is_action_pressed(dir):
	#            move(dir)
	#            attempt_move(dir)
				var target_position = world.request_move(self, inputs[dir])
				if target_position:
					move_to(target_position)
					steps_counter += 1
					emit_signal("step_taken")

#func attempt_move(direction):
#    ray.cast_to = inputs[direction] * tile_size
#    ray.force_raycast_update()
#    if !ray.is_colliding():
##        position += inputs[dir] * tile_size
#        move_to(position + inputs[direction] * tile_size)


func move_to(target_position):
#    pause = true
	# TODO double check this is working
	set_process_unhandled_input(false)
#    $AnimationPlayer.play("walk")

	# Move the node to the target cell instantly,
	# and animate the sprite moving from the start to the target cell
#    var move_direction = (target_position - position).normalized()

	move_tween.interpolate_property(
		self,"position",
		position,target_position,
		1.0 / speed,
		Tween.TRANS_LINEAR, Tween.EASE_IN)

	move_tween.start()

	# Stop the function execution until the animation finished
#    yield($AnimationPlayer, "animation_finished")
	yield(move_tween, "tween_completed")

#    if !falling and !world.flashing:
#        pause = false
	set_process_unhandled_input(true)
	
func fall():
	# TODO why does set process false not work??
#    set_process_unhandled_input(false)
	pause = true
	falling = true
#    fall_tween.connect("tween_completed", world, "on_FallTween_completed")
	
	fall_tween.interpolate_property(self, "rotation_degrees", rotation_degrees, 720, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	fall_tween.interpolate_property(self, "scale", scale, Vector2(0.1, 0.1), 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	fall_tween.start()
	
	yield(fall_tween, "tween_all_completed")
	
	rotation_degrees = 0

	falling = false

#    set_process_unhandled_input(true)
	# this will be unpaused by world
#    pause = false
	

func descend():
	pause = true
#    $Sprite.play("shine")
	stairs_tween.interpolate_property(self, "scale", scale, Vector2(0.8, 0.8), 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	stairs_tween.start()
	
	
func pause_set(new_value):
	pause = new_value
	match new_value:
		false:
			$Sprite.play("shine")
		true:
			$Sprite.play("loading")

func _on_Shine_timeout():
	pass
#    print("shine")
#    $Sprite.play("shine")


func _on_Sprite_animation_finished():
	if pause:
		$Sprite.frame = 0
	else:
		$Sprite.stop()
		$Sprite.frame = 0
