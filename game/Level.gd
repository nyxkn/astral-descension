extends TileMap
class_name Level

onready var world = get_parent()
onready var textbox = world.textbox
onready var player = world.player

var screens_seen = []

func _ready():
	setup()

func setup():
#    print(object)
#    world.connect("screen_changed", self, "_on_screen_changed")
	world.connect("screen_changed", self, "on_screen_changed_parent")
	world.connect("event_triggered", self, "_on_event_triggered")

func on_screen_changed_parent():
	print("screen changed to ", str(world.current_screen))
	if screens_seen.count(world.current_screen) == 0:
		screens_seen.push_back(world.current_screen)
		_on_screen_changed()

func _on_screen_changed():
	print("parent _on_screen_changed")

	match world.current_screen:
		Vector2(0, 0):
#            for i in 4: yield(player, "step_taken")
			textbox.set_text([
				""
				])

func _on_event_triggered():
	print("event triggered")
	
	textbox.set_text([
		""
		])   
	
