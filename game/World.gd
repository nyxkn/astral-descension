extends Node

signal screen_changed
signal event_triggered

const FIRST_LEVEL = 0
const CHEAT = 0

const PATH_LEVELS = "res://game/levels/"
var levels = []

var current_screen = Vector2.ZERO
var current_level_id
var current_level: TileMap

var flash_period = 0
var timer = 0
var flashing = false
var flash_counts = 0
var flash_done_counter = []
var flash_done = []
var flash_path

var objectives = 0
var objectives_done = []

#onready var text_box = $TextBox
onready var camera: Camera2D = $Camera2D
onready var camera_tween = camera.get_node("Tween")

onready var player = $Player

onready var textbox = $TextBox

onready var LevelIndicator = preload("res://game/entities/LevelIndicator.tscn")
onready var LevelIndicators = $Camera2D/UI/LevelIndicators
#onready var SeenLevel = preload("res://game/entities/SeenLevel.tscn")
#onready var CurrentLevel = preload("res://game/entities/CurrentLevel.tscn")



func _ready():
	levels = get_levels()
	flash_done.resize(levels.size())
	flash_done_counter.resize(levels.size())
	for i in flash_done.size():
		flash_done[i] = {}
	objectives_done.resize(levels.size())
	for i in levels.size():
		var level_status = LevelIndicator.instance()
		LevelIndicators.add_child(level_status)
		level_status.rect_position.y = 8 * i
	
	change_level(FIRST_LEVEL)
	player.fall_tween.connect("tween_all_completed", self, "on_FallTween_completed")
	player.stairs_tween.connect("tween_all_completed", self, "on_StairsTween_completed")
#    current_level_id = -1
#    next_level()

	textbox.connect("textbox_reading", self, "on_textbox_reading")
	textbox.connect("textbox_cleared", self, "on_textbox_cleared")

	$Transition/ColorRect.show()
	$Transition/AnimationPlayer.play("fade_in")
	yield($Transition/AnimationPlayer, "animation_finished")
	player.pause = false

#    $BGM.play()

	# our textbox holds 110 char of quick brown fox. it's going to be less with longer words
#    $TextBox.queue_text("The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.")


func trigger_event():
	if ! objectives_done[current_level_id]:
		player.pause = true
		emit_signal("event_triggered")
		objectives += 1
		objectives_done[current_level_id] = true



func get_levels():
	var levelDir: Directory = Directory.new()
	var levelsArray = []
	
	if levelDir.open(PATH_LEVELS) == OK:
		levelDir.list_dir_begin()
		var file_name = levelDir.get_next()
		while file_name != "":
			if file_name.ends_with(".tscn"):
				levelsArray.append(file_name)
			file_name = levelDir.get_next()
			
	levelsArray.sort()
	return levelsArray
	
func next_level():
#    yield(player.move_tween, "tween_completed")
#    yield(get_tree().create_timer(0.2), "timeout")
	change_level(current_level_id + 1)
	$Transition/AnimationPlayer.play("fade_in", -1, 2)

func on_StairsTween_completed():
	$Transition/AnimationPlayer.play("fade_out", -1, 2)
	player.hide()
	next_level()
	yield(get_tree().create_timer(0.2), "timeout")
#    player.stairs_tween.interpolate_property(self, "scale", player.scale, Vector2.ONE, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#    player.stairs_tween.start()
	player.unfall_tween.interpolate_property(player, "scale", player.scale, Vector2.ONE, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	player.unfall_tween.start()
	player.pause = false
	player.show()
	player.get_node("Sprite").play("shine")
	

#func fall():
#    player.fall()
#    yield(player.fall_tween, "tween_completed")
#    yield(get_tree().create_timer(1), "timeout")

func on_FallTween_completed():
	$Transition/AnimationPlayer.play("fade_out", -1, 2)
	yield(get_tree().create_timer(0.2), "timeout")
	next_level()
	yield(get_tree().create_timer(0.4), "timeout")
	player.unfall_tween.interpolate_property(player, "scale", player.scale, Vector2.ONE, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	player.unfall_tween.start()
	yield(get_tree().create_timer(0.2), "timeout")
	player.pause = false

func change_level(level_id):
	print("changing level")

	var Level: PackedScene = load(PATH_LEVELS + levels[level_id])
	var level = Level.instance()
	
	level.get_node("Triggers").hide()
	
	add_child_below_node($LevelPlaceholder, level)
	
	if current_level:
		remove_child(current_level)
		current_level.queue_free()
	
	current_level = level
	current_level_id = level_id


	current_level.get_node("Path").visible = false
	var path2 = current_level.find_node("Path2")
	if path2: path2.visible = false
#    flash_done[current_level_id] = false
	flash_done_counter[current_level_id] = 0
	
	LevelIndicators.get_child(current_level_id).get_node("Seen").visible = true

	camera_reset()
	
	player.position = Vector2.ZERO + current_level.cell_size / 2
	
	var map = current_level.get_node("Triggers")
	var used_triggers = map.get_used_cells()
	for tilepos in used_triggers:
		var tile_id = map.get_cellv(tilepos)
		if tile_name(tile_id) == "spawn":
			player.position = tile_coords_to_pixel(tilepos)

func tile_name(tile_id):
	return current_level.tile_set.tile_get_name(tile_id)

func tile_coords_to_pixel(coords):
	return(current_level.map_to_world(coords) + current_level.cell_size / 2)

func request_move(entity, direction):
	var map = current_level
	
	var cell_start = map.world_to_map(entity.position)
	var cell_target = cell_start + direction

	var tile_id = map.get_cellv(cell_target)
	var tile_id_path = map.get_node("Path").get_cellv(cell_target)
	var path2 = map.find_node("Path2")
	var tile_id_path2 = path2.get_cellv(cell_target) if path2 else 0
	var tile_id_trigger = map.get_node("Triggers").get_cellv(cell_target)
	var tile_id_pathtrigger = map.get_node("PathTrigger").get_cellv(cell_target)
	var tile_name = map.tile_set.tile_get_name(tile_id)
#    print(tile_name)
	
 
#    if tile_id > 0 or $Path.get_cellv(cell_target) == 3:

	if tile_name(tile_id_pathtrigger) == "path":
		start_flashing(str(cell_target.x, cell_target.y))

	if tile_name(tile_id).left(6) == "stairs":
		player.descend()
#        next_level()
	
	if tile_name(tile_id_trigger).left(6) == "stairs":
		if ! CHEAT:
			player.fall()
		return(tile_coords_to_pixel(cell_target))

	if tile_name(tile_id_trigger) == "talk":
		trigger_event()
	
#    if tile_name(tile_id_trigger) == "angel":
#        return()
#
	# if we move to any other tile
	if (tile_id > 0 or tile_id_path > 0 or tile_id_path2 > 0) and (tile_name(tile_id_trigger) != "angel"):
		
		# don't update position OR camera movement if camera is already moving
		if ! camera_tween.is_active():
			# rewrite this so that it only moves in the direction we're going out of. or maybe lock the camera until tween is complete
			if (cell_target.x > 9 + current_screen.x * 10 || cell_target.x < 0 + current_screen.x * 10 ||
				cell_target.y > 9 + current_screen.y * 10 || cell_target.y < 0 + current_screen.y * 10):
				camera_move(direction)
				
#            return(map.map_to_world(cell_target) + map.cell_size / 2)
			return(tile_coords_to_pixel(cell_target))

func camera_reset():
	camera.position = Vector2.ZERO
	current_screen = Vector2.ZERO
	print("screen changed reset")
	emit_signal("screen_changed")

	

func camera_move(direction):
	print("screen change")
	
#    var tween = get_node("../Tween")
	camera_tween.interpolate_property(camera, "position", camera.position, camera.position + direction * 160, 0.8, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	camera_tween.start()
#    camera.position += direction * 160

	current_screen += direction
	emit_signal("screen_changed")
	flash_done_counter[current_level_id] = 0


func on_textbox_cleared():
	player.pause = false
	
func on_textbox_reading():
	player.pause = true

func start_flashing(cell_id):
	if ! flash_done[current_level_id].has(cell_id):
#    if ! flash_done[current_level_id][cell_id]:
		flashing = true
		flash_counts = 0
		player.pause = true
		flash_done[current_level_id][cell_id] = true
		
		print(flash_done_counter[current_level_id])
		
		match flash_done_counter[current_level_id]:
			0: flash_path = current_level.get_node("Path")
			1: flash_path = current_level.get_node("Path2")
			
		flash_done_counter[current_level_id] += 1
				
		# delay the start for effect
#        timer = -0.5
#        yield(get_tree().create_timer(0.5), "timeout")
		
#        current_level.get_node("Path").visible = true

		flash_period = 0.3
#        timer = 0

func _process(delta):
	if current_level:
#        var path = current_level.get_node("Path")
		
		
		if flashing:
			timer += delta
#            if timer > float(flash_period) * (2 / (flash_counts + 1)):
			if timer > flash_period:
		#        emit_signal("timeout")
				flash_path.visible = !flash_path.visible
				# Reset timer
				timer = 0
				flash_counts += 1
				if !flash_path.visible:
					flash_period = 0.3
				else:
					flash_period = 0.2 * flash_counts
#                flash_period += 0.15 * flash_counts
				if flash_counts == 2 * 3:
					print("flash finished")
					flashing = false
					player.pause = false
