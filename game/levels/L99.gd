extends Level

func _ready():
	randomize()

func _on_screen_changed():
	
	match world.current_screen:
		Vector2(0, 0):
			for i in 1: yield(player, "step_taken")
			textbox.set_text([
				"So your journey comes to an end.",
				"You stand in front of the jury.",
				"Go and seek the verdict.",
				])
				
func _on_event_triggered():
#    textbox.set_text([])
	
#    world.objectives = 5
	
	if world.objectives < 3:
		textbox.set_text([
			"THE JURY PROCLAIMS...",
			"You are not worthy.",
			"Your journey must ends here. You cannot come with us.",
			"Farewell.",
			])
		yield(textbox, "textbox_cleared")
		$Angels.hide()
		$Triggers.set_cellv(Vector2(5,5), -1)
		
		for i in 10: yield(player, "step_taken")
		textbox.set_text([
			"There is nothing more to see.",
			"You have failed the tests.",
			"You're sentenced to spend all of eternity on this plane.",
			"- BAD ENDING - #1 of 3",
			])
			
	elif world.objectives < 5:
		textbox.set_text([
			"THE JURY PROCLAIMS...",
			"You have done well.",
			"Yet, you cannot ascend to the realm of the gods.",
			"We shall come back and keep you company, now and then.",
			"- NORMAL ENDING - #2 of 3",
			])
		yield(textbox, "textbox_cleared")    
		$Timer.start()
		$Angels/Angel1.hide()
		$Triggers.set_cellv(Vector2(5,5), -1)
		$Angels/Angel2.hide()
		$Angels/Angel5.hide()
			
	else:
		textbox.set_text([
			"THE JURY PROCLAIMS...",
			"Congratulations!",
			"You have made it. You've completed the trial impeccably.",
			"Now come. It is time for your initiation. The gods will soon welcome you amongst them.",
			"- GOOD ENDING - #3 of 3"
			])       
		yield(textbox, "textbox_cleared")
		set_cellv(Vector2(4,6), tile_set.find_tile_by_name("path"))      
		set_cellv(Vector2(5,6), tile_set.find_tile_by_name("path"))      
		set_cellv(Vector2(6,6), tile_set.find_tile_by_name("path"))      
		set_cellv(Vector2(6,5), tile_set.find_tile_by_name("path"))      
		set_cellv(Vector2(6,4), tile_set.find_tile_by_name("path"))      


func _on_Timer_timeout():
	var angel = $Angels.get_child(randi() % $Angels.get_child_count())
	angel.visible = !angel.visible
		
