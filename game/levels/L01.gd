extends Level

func _on_screen_changed():
	match world.current_screen:
		Vector2(0, 0):
			for i in 4: yield(player, "step_taken")
			textbox.set_text([
				"Careful now. These planes aren't going to be as forgiving as the previous one.",
				"Falling is painful.",
				])

func _on_event_triggered(): 
	textbox.set_text([
		"You have shown courage. I shall make note of it.",
		"You may take the steps down. All is done.",
		])

