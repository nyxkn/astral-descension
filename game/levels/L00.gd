extends Level

func _on_screen_changed():
#    print("child: screen changed to ", str(world.current_screen))

	match world.current_screen:
		Vector2(1, 0):
			for i in 4: yield(player, "step_taken")
			textbox.set_text([
				"Welcome to the astral planes!\nPress ENTER to read more, traveller.",
				"This is a dangerous place. It is a place of challenge, and you shall prove your worth.",
				"Follow the arrow and your journey shall begin.",
				])
		Vector2(1, -1):
			for i in 6: yield(player, "step_taken")
			textbox.set_text([
				"The Void.",
				"Look at this chasm. It must be crossed.",
				"The magical tile will show you the way. But the power left in these realms is scarce.",
				"The path will be shown only once and fade away soon after. You must ignore your fears and cross at all costs."
				])

func _on_event_triggered(): 
	textbox.set_text([
		"You will now begin the real journey. Go, descend and discover yourself.",
		"But you must first know: once fallen, there is no returning.",
		"What is missed won't be found again.",
		"Good luck, traveller."
		])   
