extends Level

func _on_screen_changed():
	match world.current_screen:
		Vector2(0, 0):
			for i in 5: yield(player, "step_taken")
			textbox.set_text([
				"At the end, at last."
				])
				
func _on_event_triggered(): 
	textbox.set_text([
		"Past these stairs awaits your trial. May the gods be with you.",
		"Count me among your supporters."
		])   
