tool
extends EditorPlugin

const BASE_PATH = "res://addons/nframework/"
const AUTOLOADS_PATH = BASE_PATH + "autoloads/"

const autoloads = {
    "Config": AUTOLOADS_PATH + "Config.gd",
    "Game": AUTOLOADS_PATH + "Game.gd",
    "MenuEvent": AUTOLOADS_PATH + "MenuEvent.gd",
    "Utils": AUTOLOADS_PATH + "Utils.gd",
}

var dock: ScrollContainer

func _enter_tree():
    # Initialization of the plugin goes here.
    
    for key in autoloads.keys():
        add_autoload_singleton(key, autoloads[key])
        
    # Load the dock. Load from file here so it's reloaded on every _enter_tree ?
    dock = preload("res://addons/nframework/dock/Dock.tscn").instance()
    add_control_to_dock(DOCK_SLOT_LEFT_BR, dock)
#    add_control_to_bottom_panel(dock, "NFramework")

func _exit_tree():
    # Clean-up of the plugin goes here.
    
    for key in autoloads.keys():
        remove_autoload_singleton(key)
        
    # Remove the dock.
    remove_control_from_docks(dock)
#    remove_control_from_bottom_panel(dock)
    dock.queue_free()

#var timer = 0
#func _process(delta):
#    timer += delta
#    if timer > 5:
#        timer = 0
#        print("dock_update")
