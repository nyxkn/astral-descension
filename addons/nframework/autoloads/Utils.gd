extends Node

# Utility functions we find ourselves rewriting often

func array_to_printable_string(array):
	var string := ""
	for e in array:
		string += str(e, ", ")
	return string.substr(0, string.length() - 2)
