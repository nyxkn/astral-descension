extends Node

# this could be renamed as Framework, or N
# so you get things like Framework.change_scene()

# export wouldn't work from here. it should be a scene
#export (String, FILE, "*.tscn") var MAIN_MENU: String = "res://addons/nframework/screens/MainMenu.tscn"
# or this should point to defaults, but a config file load could override this
# or poor man's version we just override this manually in game code


func change_scene(scene):
#    SceneManager.goto_scene("res://scenes/screens/MainMenu.tscn")
	get_tree().change_scene(scene)
