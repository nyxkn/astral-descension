extends CanvasLayer

# https://www.youtube.com/watch?v=QEHOiORnXIk
# this needs to be fed one piece of text at a time
# you also have to make sure that you don't feed it too long of a line as it will be clipped
# maybe once you settle on a resolution and font, determine the max amount of characters and give out a warning when longer text is added
#
# an alternative approach would be to have it read the complete text and automatically split it in paragraphs - sounds hard

# simply call queue_text() or set_text() - textbox will start reading out the text

signal textbox_cleared
signal textbox_reading
signal textbox_finished

const CHAR_READ_RATE = 0.05

onready var ui_TextBoxContainer = $TextBoxContainer
onready var ui_StartSymbol = $TextBoxContainer/MarginContainer/HBoxContainer/StartSymbol
onready var ui_EndSymbol = $TextBoxContainer/MarginContainer/HBoxContainer/EndSymbol
onready var ui_Text = $TextBoxContainer/MarginContainer/HBoxContainer/Text

enum State {
	READY,
	READING,
	FINISHED
   }

var current_state = State.READY
var text_queue := []

func hide_textbox():
	ui_StartSymbol.text = ""
	ui_EndSymbol.text = ""
	ui_Text.text = ""
	ui_TextBoxContainer.hide()

func show_textbox():
	ui_StartSymbol.text = "*"
	ui_TextBoxContainer.show()

func display_next_text():
	ui_EndSymbol.text = ""
	var next_text = text_queue.pop_front()
	ui_Text.text = next_text
	ui_Text.percent_visible = 0.0
	change_state(State.READING)
	$Tween.interpolate_property(ui_Text, "percent_visible", 0.0, 1.0, len(ui_Text.text) * CHAR_READ_RATE, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func change_state(next_state):
	current_state = next_state
	match current_state:
		State.READY:
			emit_signal("textbox_cleared")
		State.READING:
			emit_signal("textbox_reading")
		State.FINISHED:
			emit_signal("textbox_finished")
#    print(State.keys()[current_state])

func queue_text(next_text):
	text_queue.push_back(next_text)

func set_text(text_array):
	text_queue = text_array

func _ready():
#    change_state(State.READY)
	hide_textbox()

func _process(delta):
	match current_state:
		State.READY:
			if !text_queue.empty():
				show_textbox()
				display_next_text()
		State.READING:
			if Input.is_action_just_pressed("ui_accept"):
				$Tween.stop_all()
				ui_Text.percent_visible = 1.0
				text_finished()
		State.FINISHED:
			if Input.is_action_just_pressed("ui_accept"):
				if text_queue.empty():
					change_state(State.READY)
					hide_textbox()
				else:
					display_next_text()

func text_finished():
	ui_EndSymbol.text = "v"
	change_state(State.FINISHED)   

func _on_Tween_tween_completed(object, key):
	text_finished()
