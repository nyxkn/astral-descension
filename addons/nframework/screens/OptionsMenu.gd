extends MarginContainer

onready var ControlsList = find_node("ControlsList")
onready var ControlsListEntry = preload("res://addons/nframework/ui/ControlsListEntry.tscn")
onready var RebindPopup: Popup = find_node("RebindPopup")

# this is a better system of storing things than looping and comparing names!
var refs_ControlsListEntry = {}

func _ready():
	for action in Config.actions.keys():
		# TODO: better handling of multiple controls per action
		var event = Config.actions[action][0]
		var entry = ControlsListEntry.instance()
		ControlsList.add_child(entry)
		refs_ControlsListEntry[action] = entry
		entry.get_node("Action").text = str(action, ":")
		update_bind(action, event)

func update_bind(action, event) -> void:
	var entry = refs_ControlsListEntry[action]
	entry.get_node("Panel/Rebind").text = event.as_text()
	entry.get_node("Panel/Rebind").connect("pressed", self, "_on_Rebind_pressed", [action])    

func _on_Rebind_pressed(action) -> void:
	RebindPopup.popup_centered()
	yield(RebindPopup, "NewControl")
	if RebindPopup.new_event == null:
		return
	var event: InputEvent = RebindPopup.new_event
	# TODO: for multiple controls per action you'd do a push_back here, not a replace
	Config.actions[action] = event
	InputMap.action_add_event(action, event)
	update_bind(action, event)    
